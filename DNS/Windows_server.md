# Configuring a Windows 2019 DNS Server 💬

<!-- TOC -->

- [Configuring a Windows 2019 DNS Server 💬](#configuring-a-windows-2019-dns-server-)
    - [Introduction 📜](#introduction-)
    - [Setting a static IP address 📺](#setting-a-static-ip-address-)
    - [Installing the server 💿](#installing-the-server-)
        - [Setting up the Forward Lookup Zone ⏩](#setting-up-the-forward-lookup-zone-)
        - [Adding a Reverse Lookup Zone 🔙](#adding-a-reverse-lookup-zone-)
        - [Arranging Hosts 📚](#arranging-hosts-)
        - [Editing Forwarders 🔭](#editing-forwarders-)
    - [Configurations for a test client 🌐](#configurations-for-a-test-client-)
    - [Testing 🧪](#testing-%F0%9F%A7%AA)

<!-- /TOC -->

## Introduction 📜

A DNS Server is used for translating IP addresses into URLs and vice-versa. Let's try to set one up for our server. We will again need two network adapters: NAT and M117_Lab, so we can manage our internal network and get access to the Internet. If our DNS server doesn't have a static IP address, our clients could have problems when trying to connect to it. So here's how we set a static IP:

## Setting a static IP address 📺

Click on the internet icon on the bottom right of the screen > Select the network you're connected to (in my case it's "Unidentified network")> "Change adapter options" and right click the "unidentified network". Hint: that's the network card for our internal network where we will set the static IP address. Right click it, go to properties and open "Internet Protocol version 4" and edit it with the settings shown below:

![Alt text](/DNS/images/image.png)

Choosing 127.0.0.1 as our prefered DNS server might seem a bit weird, but this is the loopback address, which means the machine is supposed to use its own DNS service to resolve domain names. Make sure both NAT and M117_Lab has this as the prefered DNS, or else it will default to Google and won't be able to find its own hosts (which took me a little troubleshooting).

Now that our server has its static IP we can move on to installing the DNS server.

## Installing the server 💿

For the first step, go to the Dashboard and click on "Add roles and features" > "Role-based or feature-based installation" > "Select a server from the server pool" and select the current computer > Tick "DNS Server" > Continue pressing "Next" until it prompts you to install the features and restard the Server. If this is what the installation screen looks like, we can move on to the next step.

![Alt text](/DNS/images/image-1.png)

Now that our Dashboard has a DNS server, let's open that, right click the server and select "DNS Manager" where we will set up most of our server's features.

### Setting up the Forward Lookup Zone ⏩

Let's start with a forward lookup zone, which translates domain names into IP addresses.

On the sidebar on the left, right click the windows server and click "New Zone" > "Primary Zone" > "Forward Lookup Zone" > Fill up the Zone Name field with "sota.ch", since that's the name of the fictional company we work for and create a new file with that name > "Do not enable dynamic updates" > Finish.

![Alt text](/DNS/images/image-5.png)

### Adding a Reverse Lookup Zone 🔙

Now we also need something to translate IP addresses into domain names. That's where the Reverse Lookup Zone comes in:

Repeat the previous process for adding a zone but select "Reverse lookup zone" instead of "Forward Lookup Zone" this time. From then on, select "IPv4 Lookup Zone" > and fill out "192.168.100" for the Network ID > "Create a new file with this name" > Deny automatic updates again. Below is the finished configuration.

![Alt text](/DNS/images/image-3.png)

### Arranging Hosts 📚

The host records associates hostnames with an IP address (example.com to 10.1.1.1) so that any device on the network can just type in example.com and be directed to 10.1.1.1. Let's get this set up:

In the DNS Manager, go to the sidebar on the left and open "Server"" > "Forward Lookup Zones" > right click "sota.ch" on the dropdown menu and select "New Host (A or AAAA)", where we will set up a new host named "web" with "192.168.100.160" as the IP address before ticking the "Create associated pointer (PTR) Record" box.

Let's also add another Host for our client. For this record, let's use a creative name like "client" and assign it to the IP address "192.168.100.50".

![Alt text](/DNS/images/image-10.png)
![Alt text](/DNS/images/image-11.png)

### Editing Forwarders 🔭

Our DNS server will often not be able to solve domain name requests, for example when a client looks up a website which is not in our network, like youtube.com. To address this issue, we will provide our server with other DNS addresses it can forward the requests we cannot solve locally.

As of 2024, the most widely-used public DNS solvers are Google and Cloudlfare, so those are the providers we will use.

In the DNS Manager, let's right click our server in the sidebar on the left again. Under properties, select the Forwarders tab and  click on "Click here to add an IP address or DNS name" and add both 1.1.1.1 and 8.8.8.8. Select OK then hit Apply.

![Alt text](/DNS/images/image-4.png)

## Configurations for a test client 🌐

The only thing we must change on both network cards on our Windows 10 client is the prefered DNS server to be 192.168.100.160. This means that when the client sends out a request for a website (ex. web.sota.ch), it will first check our DNS server for local domains instead of going straight to Google, where its request for an internal domain will not be fulfilled. Here's how we do this:

For the client, click on the internet icon on the bottom right of the screen > Select the network you're connected to (in my case it's "Unidentified network")> "Change adapter options" and right click the "unidentified network". Go to "properties and open "Internet Protocol version 4" and edit it to match the settings shown below:

![Alt text](/DNS/images/image-12.png)

For the other Network that isn't "unidentified" (aka our NAT card), we only need to change the prefered DNS server to 192.168.100.160 and the alternate DNS server to 8.8.8.8, as stated before. To do this, repeat the same process we went through to get to the unidentified network's properties. Don't mess with the automatic IP addressing, or you won't be able to connect to the Internet!

## Testing 🧪

Now that our client is set up, let's test the server by testing some of our DNS server's features by running a series of commands on command prompt.

Verify Forward Lookup Zone:

```
nslookup web.sota.ch
```

![Alt text](/DNS/images/image-6.png)

Test Reverse lookup Zone:

```
nslookup 192.168.100.160
```

![Alt text](/DNS/images/image-14.png)

Check Forwarders:

```
nslookup www.youtube.com
```

![Alt text](/DNS/images/image-8.png)

If you got the same messages as me, hooray!! You just set up your first functional DNS server!