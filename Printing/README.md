- [Back to main page](https://gitlab.com/MNatal/m123/-/blob/main/README.md?ref_type=heads)

# Printing services for Windows 2022 📠

<!-- TOC -->

- [Printing services for Windows 2022 📠](#printing-services-for-windows-2022-)
    - [Introduction 📎](#introduction-)
    - [Setting up the service 🧰](#setting-up-the-service-%F0%9F%A7%B0)
    - [Print Management Setup 📰](#print-management-setup-)
    - [Client access 👤](#client-access-)

<!-- /TOC -->

## Introduction 📎

In this scenario, we want the printers to be in a separate network from the client, so that the client has to access a print server in order to print something, using the same Windows 2019 server we set up for our DNS. But how will these clients on separate networks communicate with each other? This is what I will show you in this documentation.

Let's begin by establishing which devices have which network cards:

- Client IPs: Windows 10
  - Internal Network 192.168.100.50

- Server IPs: Windows Server 2019
  - Internal network: 192.168.100.75
  - Bridged connection (internet and printer) 172.16.17.35

  Before trying to set anything up, let's make sure they can communicate with each other by typing in this command on the client's cmd:

```
ping 192.168.100.75
```
![Alt text](image-6.png)

Don't forget to do the same on the server's side, so that we know they can both communicate with each other flawlessly.

## Setting up the service 🧰

Go to the top right and click tools. This will open a drop down menu where you will select "Print Management". Open Print management, and on the sidebar, expand Print Servers > [Your Server] and right click "Drivers" so we can add a driver. In the driver Wizard, select x64 since we will only be using x64 Windows computers and select "Windows Update" in the next window so we can see all manufacturers. Since they didn't have the printers for my HP LaserJet P2055dn printer so I'm choosing HP Universal Printing PCL 6.

![Alt text](image-2.png)

## Print Management Setup 📰

Now that we have the drivers installed, let's add the printer itself. On the print management sidebar, right click "Printers" so we can search the network for printers. This should locate the printers on our network. Select one and change its printer name for something you can remember. I'll go with "HPPrintTest". Here we can select the printer driver we downloaded earlier. As you can see below, the printer has now been added to our server's devices and printers. Let's see if we can get it on our client device as well.

![Alt text](image-4.png)

## Client access 👤

Since both our client and server are on the same network, you can simply open the client's file explorer and type in "\\[your server's IP]" (in my case \\192.168.100.75). It will prompt for your credentials before showing you the screen below:

![Alt text](image-5.png)

In case you run into problems when typing in your credentials, go back to the server and open computer management, where you can find user credentials under active directory's users and computers if you expand the users tab on the sidebar on the left.
This means you can now connect to the printer and print whatever you want!
