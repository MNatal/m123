# M123 and M117 ePortfolio

Here I will be documenting what I have learned with the networking modules taught at the TBZ, as well as solving tasks provided to me by my teacher. All of my documentations will be written in english and I will be using VMWare Workstation Pro 17.5 to set up the servers in my tutorials.

- [Dynamic Host Configuration Protocol](#dynamic-host-configuration-protocol)
    - [DHCP with Cisco Packet Tracer🌉](https://gitlab.com/MNatal/m123/-/blob/main/DHCP/Packet6.md?ref_type=heads)
    - [Filius DHCP Server Configuration📡](https://gitlab.com/MNatal/m123/-/blob/main/DHCP/filius_dhcp.md?ref_type=heads)
    - [Ubuntu DHCP Server Setup Guide📱](https://gitlab.com/MNatal/m123/-/blob/main/DHCP/ubuntu_server.md?ref_type=heads)
- [Domain Name System](#domain-name-system)
    - [Windows DNS Server Setup Guide💬](https://gitlab.com/MNatal/m123/-/blob/main/DNS/Windows_server.md?ref_type=heads)
- [Printer Services (WIP)](#printer-services-wip)