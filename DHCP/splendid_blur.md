# Creating a network configuration for a small business 🏢

<!-- TOC -->

- [Creating a network configuration for a small business](#creating-a-network-configuration-for-a-small-business)
    - [Naming and connecting 📚](#naming-and-connecting-)
    - [Configuring the DHCP Server 📻](#configuring-the-dhcp-server-)
    - [Client configuration 📠](#client-configuration-)
      - [Why not DHCP everything? 😕](#why-not-dhcp-everything-)
    - [Setting up the DNS Server 🖥](#setting-up-the-dns-server-%F0%9F%96%A5)
    - [Setting up a web server 🕸](#setting-up-a-web-server-%F0%9F%95%B8)
    - [Configuring the router 🌉](#configuring-the-router-)
    - [Testing 🧪](#testing-%F0%9F%A7%AA)

<!-- /TOC -->

## Naming and connecting 📚

Before setting anything up, we have to first connect the machines to each other and name them according to the requirements. Connect every user and sevrer to their respective switch, which in turn each go to the router. When that's done, we want to name them according to the company's specifications.

- GL: PC11 and PC12
- Buchhal: PC21 and PC22
- Einkauf: PC41 and PC42
- Logistik: PC51 and PC52
- Server: DHCP-, DNS- and Web Server

The network will then look like this:

![Alt text](./images/splendid/image-1.png)

In total, we have:

- 8 Users
  - 5 on DHCP
  - 3 static IP
- 3 Switches
- DHCP Server
- 1 Router
- Web server
- 5 Different Departments
- DNS Server
- 3 Subnets

## Configuring the DHCP Server 📻

Our DHCP Server will be key to providing IP Adresses, DNS and gateway configurations to 5 different clients so that they may access our network. These are the settings we wanna use for the server:

1. Name: DHCP Server
2. IP Address: 10.0.0.2
3. Netmask: 255.255.0.0
4. Gateway: 10.0.0.1
5. Domain Name Server: 172.16.100.3

Getting these five settings for the DHCP right are very important, because they will be provided to our clients. If it's wrong for the server, it's wrong for them as well. Getting the correct gateway is also important so that our packets get sent to other Networks through the router without issues and the DNS we are using will be configured later.

![Alt text](./images/splendid/image-3.png)

Now, let's press the "DHCP server setup" button in the bottom right. In this menu we will only have to choose the correct IP ranges, since the other configs should be carried over from the values we typed in before opening this menu. Since our gateway is 10.0.0.1 and our server's IP is 10.0.0.2, let's start our range with 10.0.0.3. Since we only have five clients that need automatic IPs, we don't need more than 5 dynamic IP Addresses, so let's set the upper bound of address to 10.0.0.8.
This is what the setup will look like before we tick the "Activate DHCP" box and press OK:

![Alt text](./images/splendid/image-4.png)

## Client configuration 📠

Since clients 11, 12, 21, 22, 41 and 42 all get their network configs automatically from the server, all we need to do for their setuop is clicking on each of them and selecting the "Use DHCP for configuration" box in the bottom right. This will gray out most other boxes, which is exactly what we want. Here's an example of a client receiving its configurations from the server automatically:

![Alt text](./images/splendid/image-5.png)

For the other two clients which don't get their configurations automatically, namely 51 and 52, we must do this process manually. Here are the settings we want to use for them:

- IP (PC 51): 192.168.1.51

- IP (PC 52): 192.168.1.52

- Netmask (both): 255.255.255.0

- Gateway (both): 192.168.1.1

- DNS (both): 172.16.100.3

PC 51 looks like this:

![Alt text](./images/splendid/image-6.png)

PC 52 will look the exact same, except for the IP address, which is listed above.

## Why not DHCP everything? 😕

After setting up the clients' configuration manually, you must be wondering why we didn't jsut add the settings to the DHCP server for it to automatically assign all the configurations to its respective devices. The answer is quite simple. I spent over an hour trying to figure out how the router's forwarding table works, but sadly, most requests never got past the router and its automatic routing option. I even tried manually setting up its forwarding table, but nothing ever worked. I even tried setting one of the destination addresses to 255.255.255.255, figuring that the first step of the DORA process involves sending a broadcast request to the entire network, and routing that to 10.0.0.1 (which is the DHCP's gateway), but that didn't work either. ChatGPT was, like most of the times, completely useless. Most attempts were carried out in a very simple testing environment too, which only included a DHCP server, a router and a client. Filius' has finally made the first ever router that doesn't actually route anything using its forwarding table. Trust me, I've tried everything and no, it does not work. Now let's get back to the setup.

## Setting up the DNS Server 🖥

Let's give the DNS Server the following configurations:

- IP: 172.16.100.3
- Netmask: 255.255.0.0
- Gateway: 172.16.100.1
- DNS: 172.16.100.3

It should look like this:

![Alt text](./images/splendid/image-7.png)

Now, to activate the DNS server we must switch to simulation mode by clicking the green play button on top of the screen and selecting our server. In software installation, install a DNS server, apply changes and open it after going back to the home screen. In here, we want to write every single static IP address and give it a name. This is how I did it:

- dns = 172.0.0.1
- pc51 = 192.168.100.4
- web = 172.16.100.4
- pc52 = 192.168.1.52
- dhcp = 10.0.0.2
- pc 11 = 10.0.0.11

If you're done with typing in the domains and their respective IPs, select the "start" button on the top left before closing the window.

![Alt text](./images/splendid/image-12.png)


## Setting up a web server 🕸

Let's go back to the design mode and select the web server located right beside our DNS server. This is how it should be configured:

- IP: 172.16.100.4
- Netmask: 255.255.0.0
- Gateway: 172.16.100.1
- DNS: 172.16.100.3

![Alt text](./images/splendid/image-9.png)


Now, back into simulation mode, let's select the web server PC and install a webserver onto it. Apply changes, go back to the server's home screen and open the newly installed web server application. Press "start" on the top left and close the window. We now need to go back to design mode so we can set up one last thing.

![Alt text](./images/splendid/image-11.png)

## Configuring the router 🌉

This device will allow all our subnets to communicate with each other flawlessly by routing their queries to one anoter. But before we configure the router, let's make sure every single device on our network has IP forwarding enabled. I haven't tested the network without this, but it sounds important and related so let's turn it on. Now we can select the router and give each of its interfaces the correct configuration. So, the first one we will do is the cable that goes to our 10.0.0.0 network on the left. Set its IP to 10.0.0.1 and its netmask to 255.0.0.0. The second one we'll do communicates with the two servers we set up earlier, namely 172.16.100.0. This interface should have 172.16.100.1 as its IP and 255.255.0.0 as the subnet. For the last interface, which communicates with PC51 and PC52 on entwork 192.168.1.0, will use 192.168.1.1 for its IP and 255.255.255.0 as its subnet.

![Alt text](./images/splendid/image-13.png)

## Testing 🧪

To make sure all our servers and services are working, let's test them our using a computer with a random DHCP IP address. I'll use PC 41 and install both a web browser and a command line on it. Now we can test out the static IP addressing for PC11 with the following command:

```
ping 10.0.0.11
```

![Alt text](./images/splendid/image-14.png)

For the next step, let's test something outside of our subnet that isn't the DHCP server, like the DNS server. Here's what we want to write into CMD:

```
ping dns
```
![Alt text](./images/splendid/image-15.png)

Now that we know our DNS is working, we can ping the static IP PCs in 192.168.1.1 with these two commands:

```
ping pc51
```
```
ping pc52
```

![Alt text](./images/splendid/image-16.png)

And finally, we can test out the web server by opening the web browser and searching "web" in it. This should return us this page:

![Alt text](./images/splendid/image-17.png)

Now we can be 100% sure that our network is working!
