# DHCP

Here you can access all documentations related to DHCP servers that i've written in order:

- [Back to main page](https://gitlab.com/MNatal/m123/-/blob/main/README.md?ref_type=heads)
  - [DHCP with Cisco Packet Tracer🌉](https://gitlab.com/MNatal/m123/-/blob/main/DHCP/Packet6.md?ref_type=heads)
  - [Filius DHCP Server Configuration📡](https://gitlab.com/MNatal/m123/-/blob/main/DHCP/filius_dhcp.md?ref_type=heads)
  - [Ubuntu DHCP Server Setup Guide📱](https://gitlab.com/MNatal/m123/-/blob/main/DHCP/ubuntu_server.md?ref_type=heads)
  - [Creating a network configuration for a small business 🏢](https://gitlab.com/MNatal/m123/-/blob/main/DHCP/splendid_blur.md?ref_type=heads)
  