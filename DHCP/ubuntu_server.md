# Ubuntu 22.04 DHCP Server Setup Guide tool 📱

## Table of Contents
<!-- TOC -->

- [Ubuntu 22.04 DHCP Server Setup Guide tool 📱](#ubuntu-2204-dhcp-server-setup-guide-tool-)
    - [Table of Contents](#table-of-contents)
    - [Introduction 📜](#introduction-)
    - [Initial server setup 💿](#initial-server-setup-)
    - [DHCP Server installation 🔨](#dhcp-server-installation-)
    - [Finding the network interface 🔍](#finding-the-network-interface-)
    - [DHCP interface selection 🎨](#dhcp-interface-selection-)
    - [Changing network parameters 🧰](#changing-network-parameters-%F0%9F%A7%B0)
    - [Netplan configuration 🔩](#netplan-configuration-)
    - [Enabling the DHCP-Server ✅](#enabling-the-dhcp-server-)
    - [Testing 🧪](#testing-%F0%9F%A7%AA)

<!-- /TOC -->n configuration 🔩](#netplan-configuration-)
    - [Enabling the DHCP-Server ✅](#enabling-the-dhcp-server-)
    - [Testing 🧪](#testing-%F0%9F%A7%AA)

<!-- /TOC -->

## Introduction 📜

DHCP Services are essential to making internet access available for everyone, including those who don't know much about technology. In this document I will be documenting my experience and describing the process of getting a DHCP server to run on a 22.04 Linux Ubuntu Server on VMWare 17.0.

## Initial server setup 💿

First thing you'll have to do is follow a lengthy start process, where you will select language, keyboard, Ubuntu server, proxy, disks to be used, ubuntu pro and more until we reach the profile setup. This is an important step where you will type in the name for your server, your username and password.

Since this is just a test and won't actually be used for serious IP addressing, I will be picking very simple and easy-to-guess credentials.

Username: max Password: 123
![Alt text](./images/dhcpcredentials.png)

One of the settings you will come across is "Network connections" which shows you both the network cards you have installed, specifially one that manages the network and another one that connects to the internet (NAT). The one without an IP address (in my example ens37) is the one we will use as the DHCP interface. If you only see one Network available, make sure the local network card (in my class it's M117_lab) is installed and restart your server.

![Alt text](./images/network_connections.png)

## DHCP Server installation 🔨

After the initial server configuration process is done, you'll want to install a DHCP server onto it. For that, we just need to run one command:

```
sudo apt install isc-dhcp-server -y
```

 This should only take some of minutes, and it might ask you to reboot when the installation and optional security updates have concluded. Once it's done you should make sure everything is up to date with the following command:

```
 sudo apt upgrade
```

## Finding the network interface 🔍

The first step in configuring our DHCP server is figuring out what network interface we're gonna use. As far as I know, this tells the DHCP server which network to manage and address IPs to. To find this out, use the terminal to run the following command:

```
ip a
```
Now we can see in the screenshot below that there are two networks available: ens33 and ens37. We can see here that ens33 has an IP-Address, while ens37 does not. This means that we want to use ens37 as an interface since it is the identifier for the local network which we want the server to manage. Attached below is a screenshot of my server's current IP info:

![Alt text](./images/ip_a.png)

## DHCP interface selection 🎨

Now that we know what network we want to manage, let's choose the interface by running the following command:
```
sudo nano /etc/default/isc-dhcp-server
```

Here we will only have to do one thing: write your local network interface's name, in my case "ens37", in the empty INTERACESV4 apostrophes so that the server knows which network to manage. If it looks like the screenshot below, press ctrl + x to exit.

![Alt text](./images/interface.png)

## Changing network parameters 🧰

After all that, now comes the cool part, configuring the DHCP server with your own parameters! Let's begin by opening the DHCP server configuration file with this command:

```
sudo nano /etc/dhcp/dhcpd.conf
```

![Alt text](./images/dhcp_file.png)

Here we want to comment (add a hashtag before) these two lines:

```
#option domain-name "example.org";

#option domain-name-servers ns1.example.org, ns2.example.org;
```

Don't forget to remove the hastag before the "authoritative" line (as depicted on the screenshot above), so that the server we're setting up has authority to manage our network.

In the next step we  will be setting up the basic setting up a basic Subnet declaration by uncommenting and editing an already existing one. Here we will be defining the subnet, netmask, range and router(s). To save changes, press ctrl+o and enter. This is what it should look like:

![Alt text](./images/simple_subnet.png)

This step will most likely involve some troubleshooting- even if you know what parameters you change, it's highly likely you'll make small syntax mistakes which will break the entire server, which is what happened to me. Here's how to check for mistakes in the config file:
```
sudo dhcpd -t -cf /etc/dhcp/dhcpd.conf
```

Running this command will tell you which line your mistake is located in so you can go correctly. Another tip: remember to add a semicolon after every argument like I did in the screenshot above!

## Netplan configuration 🔩

The final thing we must configure is the netplan. To access and edit these extra parameters, paste the following command into the terminal:
```
sudo nano /etc/netplan/00-install-config.yaml
```

In my case, the netplan was pretty much empty except for a couple of default entries (which are NOT sufficient for enabling the server). This is what it looked like when I first opened the incomplete config file:
![Alt text](./images/emptynetplan.png)

I had to build the entire netplan from scratch by myself based off other guides I found online. This is the configuration that worked for me:

![Alt text](./images/netplan.png)

Every time I tried to set up the "gateway4" variable it gave me an error saying it "had been deprecated", so the workaround was to set the "routes" instead and work with that. It redirects every packet sent to 0.0.0.0 to 192.168.100.1, which is the IP address of our server, and on-link just means that 192.168.100.1 is directly reachable on this network segment.
Here I also defined which interface gets its IPs from the DHCP (with ens37 dhcp: no and ens33 dhcp: yes) so that the internal network manages and distributes addresses for the clients while the other accesses the internet.

Just like before, you can leave this interface by saving with ctrl + O and pressing ctrl + x to leave. Apply the saved changes with the following command:

```
sudo netplan apply
```

## Enabling the DHCP-Server ✅

Now's the time to see if all that work paid off or if there is still some troubleshooting to do. We're gonna run this  command:

```
sudo systemctl start isc-dhcp-server
```

If something in the netplan config isn't right, the command above will return you an error message telling you what you need to fix. If it didn't return anything, hooray! We can go to the next step by typing in the following commands:

```
sudo systemctl enable isc-dhcp-server
```
```
sudo systemctl status isc-dhcp-server
```
As depicted below, your server should display a green "active(running)" status,and if you're attentive, you'll notice this command even lets you see the DORA process happening in real time when a client tries to connect to it! Exciting!
![Alt text](./images/activeserver.png)

IPv4 and IPv6 DHCP share the same PID files, which leads to the whole server breaking at the next startup. To prevent that, we want to disable IPv6 by running the following command:
```
systemctl disable isc-dhcp-server6
```
Now you'll be able to start the server again after it shuts down without any issues!

## Testing 🧪

Now that our DHCP server is active, let's test it out with a Windows client to make sure it's receiving the IP address it needs:

![Alt text](./images/client_test.png)

As you can see above, merely turning on a Windows client connected to our network shows that it has automatically received an IP Address (not APIPA!!) from the server we set up. Congratulations, you've just set up your first DHCP server on Ubuntu!! \o/