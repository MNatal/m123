# Lab Exercise 1 - DHCP with Cisco Packet Tracer 🌉
The objective of this exercise is to analyse DHCP configuration, observing Lease times (IP Address assignment)  und finally changing its configurations.
<!-- TOC -->

- [Lab Exercise 1 - DHCP with Cisco Packet Tracer 🌉](#lab-exercise-1---dhcp-with-cisco-packet-tracer-)
    - [Table of Contents](#table-of-contents)
    - [Reading router configuration 📑](#reading-router-configuration-)
    - [DORA - Observing DHCP Leases 🔎](#dora---observing-dhcp-leases-)
    - [Configuring a network 🔨](#configuring-a-network-)

<!-- /TOC -->

In Cisco Packet Tracer, double click the router and open its CLI (Command Line Interface) so we can read out some information. Before we do anything, we must type in "enable" to activate administrator privileges.

>For which subnet is the DHCP server active?
>>In what range does the DHCP-Server distribute IPv4 Addresses?
>>>How many IPv4 addresses can the DHCP server dynamically assign?

The command "show running-config" shows us things like the IP pool used by the DHCP server, the network's Ip-Address and Subnet, excluded addresses and the router's name. The Subnet for this network 255.255.255.0. and the IP range goes from 192.168.25.1 - 192.168.25.254, which means the DHCP server can assign a total of 254 Addresses.

![](./images/image-1.png)

>Which IP addresses are assigned and to which MAC addresses?

To retrieve a table with which IP adresses are assigned to which IP addresses, we can run the command "show ip dhcp binding"

| IP Address    | MAC Address       | Type
| -------- | ----------------- | ------------ |
| 192.168.25.29  |  0001.632C.3508 | Automatic|
| 192.168.25.32 | 0007.ECB6.4534 | Automatic| 
| 192.168.25.31|  0001.632C.3508 | Automatic | 
192.168.25.33 | 00D0.BC52.B29B | Automatic |

Here's what the command looks like in the CLI:
![](./images/image.png)

>What does the configuration "ip dhcp excluded-address" result in?

This configuration can exclude IP-Adresses from the pool. The excluded Adresses can then be used as fixed IP-Adresses for printers, routers, servers and access points.

## 2. DORA - Observing DHCP Leases 🔎

>What OP code does each part of the DORA process have?

1. Discovery: OP code 1
2. Offer: OP code 2
3. Request OP code 1
4. Acknowledge: OP code 2

>To which IP- and MAC address is the DHCP-Discover sent? What is special about these addresses?
>>Why is the DHCP discover from the switch also sent to PC3?

DHCP-Discover is sent as by the client to all devices in the network using the broadcast address 255.255.255.255 and to the destination MAC Address FF:FF:FF:FF:FF:FF. This ensures that all devices in the network receive the message, including clients in the network, who will discard the packet while DHCP-Servers will read it and reply with a DHCP-Offer.


Here are screenshots for PDU details for each DORA step in Cisco Packet Tracer.
![](./images/image2.png)

## 3. Configuring a network 🔨
The server currently receives its IPv4 address from the DHCP server. This IPv4 address is automatic and can change, for example when the router or server restarts. In principle, the router can assign another IPv4 address even after the DHCP lease time has expired.
To prevent this, we will assign the server a static IPv4 address.

Back in the [first chapter](#1-reading-router-configuration-📑), we used the command "show running-config" in the router's command line interface to see which addresses were excluded. We will now pick one of them to assign to our Server, namely "192.168.25.1".

For the first step, double click on the server and navigate to Global > Settings and set the  IPv4 Configuration to static, before typing in the IP-Address we chose in the Default Gateway field. Set IPv6 to static as well.

![](./images/image3.png)

Now let's switch to the FastEthernet0 tab on the left. There, we will set both IP- and IPv6 COnfiguration to static. Under IPv4, we will type in our selected excluded address again (192.168.25.1) and press enter, which will automatically fill out the Subnet Mask field, as shown below.

![](./images/image4.png)

Now that we'we finished the setup, let's it out by opening a random client like PC1. Open command prompt and ping the server. If you get a reply, congratulations! You have successfully set up a DHCP server. This is what a successful ping looks like:

![](./images/image5.png)


Now for the second test, navigate to Desktop and open the Web Browser, where you will type the server's IPv4 Address into the search bar. If you see the server's website, that's another confirmation that the DHCP server works.

![](./images/image6.png)