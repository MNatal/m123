
# Filius DHCP Server Configuration 📡

<!-- TOC -->

- [Filius DHCP Server Configuration 📡](#filius-dhcp-server-configuration-)
    - [Introcudtion](#introcudtion)
    - [DHCP Server Setup 🖥](#dhcp-server-setup-%F0%9F%96%A5)
        - [Configuration walkthrough 🛠](#configuration-walkthrough-%F0%9F%9B%A0)
        - [Base Settings 🔧](#base-settings-)
        - [Static Address Assignment 🔨](#static-address-assignment-)
    - [Client Setup 💻](#client-setup-)
        - [Testen 🧪](#testen-%F0%9F%A7%AA)

<!-- /TOC -->

## Introduction

Welcome to my first Markdown Documentation! Here I will attempt to cram as many markdown formatting features such as tables, Hyperlinks, images, etc in this document as possible. If you find it a bit cluttered, don't worry, my other documentations won't be too over-the-top. In this documentation, I will explain how to set up and configure a  [**DHCP server**](https://en.wikipedia.org/wiki/Dynamic_Host_Configuration_Protocol). [Here's the download](https://tbzedu.sharepoint.com/sites/campus/students/it/Forms/AllItems.aspx?id=%2Fsites%2Fcampus%2Fstudents%2Fit%2Fict23c%2FInformatikmodule%2FM123%2F02%5FDHCP%2F01%5FTheorie%5F%2B%5FAuftrag%2F02%5FHands%2Don%5FFilius%2FDHCP%5FDienst%5FFilius%5FVorlage%5FCAL%2Efls&parent=%2Fsites%2Fcampus%2Fstudents%2Fit%2Fict23c%2FInformatikmodule%2FM123%2F02%5FDHCP%2F01%5FTheorie%5F%2B%5FAuftrag%2F02%5FHands%2Don%5FFilius) for the Filius File with all the [**clients**](https://en.wikipedia.org/wiki/Client_(computing)) and [**servers**](https://en.wikipedia.org/wiki/Server_(computing)) to set up. 

## DHCP Server Setup 🖥

In this environment we are presented with a [**star network**](https://en.wikipedia.org/wiki/Star_network) including a [**DHCP server**](https://en.wikipedia.org/wiki/Dynamic_Host_Configuration_Protocol) and three [**clients**](https://en.wikipedia.org/wiki/Client_(computing)). One of the clients, namely **client 3**, has his own [**IP address**](https://en.wikipedia.org/wiki/IP_address) reserved for him, which lies outside of the network's IP range. This means he won;t be able to communicate with the two other clients in this network. To try and find a solution let's take a closer look at our setup:

- Client 1: Automatic IP-Address
- Client 2: Automatic IP-Address
- Client 3: Static IP-Address

| Gerät    | MAC Adresse       | IP Adresse
| -------- | ----------------- | ------------ |
| Client 1 | 22:15:94:7A:CC:2E | 192.168.0.150|
| Client 2 | D5:9D:B0:A0:07:7B | 192.168.0.151| 
| Client 3 | 49:11:5C:DA:77:23 | 192.168.0.50 | 

![Architektur](./images/alles.png)

### Configuration walkthrough 🛠
Now, let's select the DHCP server icon, click on *DHCP-Server setup* so we can change the *Base Settings*. 


![Architektur](./images/server%20einrichten.png)

### Base Settings 🔧
In the *Lower- und Upper bound of address*, let's fill in the IP range with a the lower and upper bounds of a list that tells the server what [**IP addresses**](https://en.wikipedia.org/wiki/IP_address) it can assign to our [**Clients**](https://en.wikipedia.org/wiki/Client_(computing)). This is the range we want to use:
1. Lower bound: 192.168.0.150
2. Upper bound: 192.168.0.170
Now only the IP addresses in the pool can be assigned to the clients. [Gateway](https://en.wikipedia.org/wiki/Default_gateway), [DNS Server](https://en.wikipedia.org/wiki/Domain_Name_System) and all other settings on this page should remain unchanged.

![Architektur](./images/base%20settings.png)

### Static Address Assignment 🔨

Now, let;s get to the *Static Address Assignment* tab. Since we were tasked to assign client 3 with a static [**IP address**](https://en.wikipedia.org/wiki/IP_address) let;s write down its [**MAC address**](https://en.wikipedia.org/wiki/MAC_address) in the first field. This will allow the server to identify which client is which and what IP it should receive. Now, let's assign this PC with a static [**IP address**](https://en.wikipedia.org/wiki/IP_address), in this case it's 192.168.0.50. Write it down and click *Add*. 

![Architektur](./images/statische%20zuweisung.png)

If it looks like the image above,we can go back to the *Base Settings* tab, click on the *Activate DHCP* button and press *OK*.

![Architektur](./images/DHCP%20aktivieren.png)

## Client Setup 💻
In contrast to setting up a [**server**](https://en.wikipedia.org/wiki/Server_(computing)), getting a  [**client**](https://en.wikipedia.org/wiki/Client_(computing)) ready to go in Filius is pretty simple and only needs a single step: ticking the  *Use DHCP for configuration* box. This should be done to every [**client**](https://en.wikipedia.org/wiki/Client_(computing)), so that they get their IP configuration from the server we set up.

![Architektur](./images/use-for-setup.PNG)

### Testen 🧪

Now that everything is set up correctly, the  [**clients**](https://en.wikipedia.org/wiki/Client_(computing)) will get their [**IP addresses**](https://en.wikipedia.org/wiki/IP_address) automatically. This can be verified by using a cleint's *Command Line* überprüfen, by pinging another [**client**](https://en.wikipedia.org/wiki/Client_(computing)). This is what a successful ping should look like:

![Architektur](./images/cmd.PNG)

Hopefully this documentation could help you understand DHCP servers better!!!
