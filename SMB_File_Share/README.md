# Ubuntu SAMBA Server Setup Guide 🎉

<!-- TOC -->

- [Ubuntu SAMBA Server Setup Guide 🎉](#ubuntu-samba-server-setup-guide-)
    - [Installing SAMBA 🎊](#installing-samba-)
    - [SAMBA Setup 🔨](#samba-setup-)
    - [Setting a static IP address 🔧](#setting-a-static-ip-address-)
    - [Creating user accounts 👥](#creating-user-accounts-)
    - [Client setup and testing 💻](#client-setup-and-testing-)

<!-- /TOC -->

## Installing SAMBA 🎊

Before anything, let's make sure that our Ubuntu server is up to date by running this:

```
sudo apt upgrade
```

This shouldn't take too long, and now that our systems are up to date we can start the installation by running the following command:

```
sudo apt install samba
```

To test whether the server is active and running, let's run this command:
```
systemctl status smbd 
```

If the output shows a green "active (running) text, it means you've successfully installed SAMBA.

![Alt text](image-1.png)

## SAMBA Setup 🔨

It can save us some time and troubleshooting if we make the server start automatically every time the system reboots, so let's type this into the terminal:
```
 sudo systemctl enable --now smbd
```

Another thing that we should already do is allow SAMBA through the firewall so that it doesn't get mistaken for anything malicious:

```
sudo ufw allow samba
```

Let's also create a new folder called "sambashare" in our home directory with with this command:

```
mkdir /home/[your username]/fileshare
```

This will be the name of the folder our Windows client will access.

Now we can get to carefully modifying the config files for samba. First, let's find the configuration file where we can change our server's parameters, accessible with this command:

```
sudo nano /etc/samba/smb.conf
```

Here we will be using nano to edit all our config files because of all the Ubuntu text editors I've used, it was the most simple one by far, especially compared to Vim. In the config file, let's scroll down until the "Share Definitions" section and change these definitions:

```
browseable = yes
read only = no
```

![Alt text](image-9.png)

Remember at the start of the installation when we wrote down the interface name being used to manage the local network? This is where that comes in, set it in the networking section like this:

```
interfaces: 127.0.0.0/8 ens34
```

![Alt text](image-4.png)

Once that's done, scroll down to the very bottom of the document so we can write some specific permissions for the fileshare folder. This is what we wanna write:

```
[sambashare]
comment = this is the config of our fileshare folder
path = /etc/[your user]/fileshare
read only = yes
browseable = yes
security = user
```

This is what the last entry in the config page should look like:

![Alt text](image-8.png)

As always, just press *Ctrl + O* and hit Enter before exiting the config file with *Ctrl + X*.

## Setting a static IP address 🔧

It's always good practice to give your server a static IP address so clients can easily ping- and access it. Since we're on the server version of Ubuntu, it's not as straightforward as it is on a desktop version of Uubuntu. Let's begin by configuring our netplan. This is the netplan config I made for our server:

```
sudo nano /etc/netplan/01-netcfg.yaml
```

We won't be using gateway4 here because every time I tried it the console gave me an error saying it "had been deprecated", so the workaround was to set the "routes" instead and work with that.

This is the netplan I made from scratch based on my other netplan I created in the DHCP setup guide:

```
network:
    ethernets:
        ens33:
            dhcp4: true
        ens34:
            addresses: [192.160.100.75/24]
            nameservers:
                addresses: [8.8.8.8, 8.8.4.4]
            routes:
            - to: 0.0.0.0/0
              via: 192.168.1.1
              metric: 100
            dhcp4: false
    version: 2

```

And this is what it should look like in the console:

![Alt text](image-5.png)

If we try to apply this netplan now, we will receive two error mesages. One of them about the permissions for the yaml file being too open and the other being about Open vSwitch not being installed. To solve this issue we will run two commands. The first one makes it so only the owner of the yaml file has full read and write access to it, and the second one downloads and installs Open vSwitch: 

```
sudo chmod 0600 /etc/netplan/00-installer-config.yaml
```

```
sudo apt install openvswitch-switch
```

Now that we have everything we need, there should be no error messages when running this command:

```
sudo  netplan apply
```

## Creating user accounts 👥

Our fileshare folder is protected by a password and username, so let's set that up with this command:

```
sudo smbpasswd -a [username]
```

For the username, fill it out with the ubuntu server username you chose earlier during setup, press enter and type in a password of your choosing.

## Client setup and testing 💻

Make sure the client is on the same network and make sure that network discovery is on, or else it won't be able to be pinged by the server. Also, set its IP to static and ensure it's in the same subnet as the server. In my case, I chose 192.168.100.50.

In the client, open file explorer and type two backslashes followed by the IP address of your server. That will then show you the "fileshare" folder we previously created. ![Alt text](image-6.png)

Attempting to open it will  prompt you for the credentials we [set up earlier](#creating-user-accounts-👥 ). After typing them in you should get full read and write access to the folder, meaning the server we set up is fully functional and accessible by anyone on the network that knows the credentials.

![Alt text](image-7.png)

Now you know how to set up and access your very own samba shared folder!
